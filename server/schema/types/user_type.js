const { GraphQLObjectType, GraphQLString, GraphQLID } = require('graphql');

const UserType = new GraphQLObjectType({
    name: 'UserType',
    fields: {
        // TODO: this means it is exposed probably change this to a username instead in future
        email: {
            type: GraphQLString
        },
        id: {
            type: GraphQLID
        }
    }
});

module.exports = UserType;