import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import currentUserQuery from '../../queries/currentUser';
import PropTypes from 'prop-types';

export default (WrappedComponent) => {
    class RequireAuth extends Component {
        componentWillUpdate(nextProps) {
            if (!nextProps.auth.loading && !nextProps.auth.currentUser) {
                this.props.history.push('/login');
            }
        }
        render() {
            return <WrappedComponent {...this.props} />
        }
    }
    RequireAuth.propTypes = {
        history: PropTypes.shape({
            push: PropTypes.func
        }).isRequired
    };

    return graphql(currentUserQuery, { name: 'auth'})(RequireAuth);
}