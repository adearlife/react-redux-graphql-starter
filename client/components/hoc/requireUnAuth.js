import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import currentUserQuery from '../../queries/currentUser';
import PropTypes from "prop-types";

export default (WrappedComponent) => {
    class RequireUnAuth extends Component {

        // due to the fact that hte refetch queries is ran at the time as the resolution on the promise
        // we cannot redirect on the response instead we have to use component will update to deal with redirect
        componentWillUpdate(nextProps) {
            if(!this.props.auth.currentUser && nextProps.auth.currentUser) {
                this.props.history.push('/dashboard');
            }
        }

        componentDidMount() {
            if(this.props.auth.currentUser) {
                this.props.history.push('/dashboard');
            }
        }
        render() {
            return <WrappedComponent {...this.props} />
        }
    }

    RequireUnAuth.propTypes = {
        history: PropTypes.shape({
            push: PropTypes.func
        }).isRequired,
        auth: PropTypes.shape({
            currentUser: PropTypes.string.isRequired
        })
    };

    return graphql(currentUserQuery, { name: 'auth'})(RequireUnAuth);
}