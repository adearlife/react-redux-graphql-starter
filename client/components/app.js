import '../assets/styles/style.scss';
import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import SignupForm from "./auth/signupForm";
import Dashboard from "./dashboard";
import LoginForm from "./auth/loginForm";
import requireAuth from "./hoc/requireAuth";
import requireUnAuth from "./hoc/requireUnAuth";
import Header from './common/header';

const App = () => {
    return (
        <BrowserRouter>
            <div>
            <Header />
            <Switch>
                <Route path="/login" component={requireUnAuth(LoginForm)} />
                <Route path="/signup" component={requireUnAuth(SignupForm)} />
                <Route path="/dashboard" component={requireAuth(Dashboard)} />
            </Switch>
            </div>
        </BrowserRouter>
    )
};

export default App;