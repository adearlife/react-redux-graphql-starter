import React, {Component} from 'react';
import AuthForm from './authForm';
import { graphql, compose } from 'react-apollo';
import mutation from '../../mutations/signup';
import query from "../../queries/currentUser";
import PropTypes from "prop-types";

class SignupForm extends Component {

    constructor(props) {
        super(props);
        this.state = { errors: []}
    }

    onSubmit({email, password}) {
        this.props.mutate({
            variables: {email, password},
            refetchQueries: [{query}]
        }).catch((res) => {
            const errors = res.graphQLErrors.map(error => error.message);
            this.setState({ errors })
        });
    }

    render() {
        return (
            <div>
                <h3>Sign Up</h3>
                <AuthForm errors={this.state.errors} onSubmit={this.onSubmit.bind(this)}/>
            </div>
        );
    }
}

SignupForm.propTypes = {
    mutate: PropTypes.func.isRequired
};

export default compose(
    graphql(mutation)
)(SignupForm);