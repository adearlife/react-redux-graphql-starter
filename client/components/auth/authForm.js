import React, {Component} from 'react';
import PropTypes from 'prop-types';

class AuthForm extends Component {
    constructor(props) {
        super(props);

        this.state = {email: '', password: ''}
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.onSubmit({
            email: this.state.email,
            password: this.state.password
        })
    }

    render() {
        return (
            <div>
                <form  onSubmit={this.onSubmit.bind(this)}>
                    <div>
                        <input
                            placeholder="Email"
                            value={this.state.email}
                            onChange={(e) => {
                                this.setState({email: e.target.value})
                            }}
                        />
                    </div>
                    <div>
                        <input
                            placeholder="Password"
                            type="password"
                            value={this.state.password}
                            onChange={(e) => {
                                this.setState({password: e.target.value})
                            }}
                        />
                    </div>
                    <div>
                        {this.props.errors.map((error) => {
                            return <div key={error}>{error}</div>
                        })}
                    </div>

                    <button>Submit</button>
                </form>
            </div>
        );
    }
}

AuthForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    errors: PropTypes.arrayOf(PropTypes.string)
};

export default AuthForm;