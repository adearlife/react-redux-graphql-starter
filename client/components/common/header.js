import React, {Component} from 'react';
import {graphql, compose} from 'react-apollo';
import {Link} from 'react-router-dom';
import auth from '../../queries/currentUser';
import logout from '../../mutations/logout';
import PropTypes from "prop-types";

class Header extends Component {

    onLogoutClick() {
        this.props.logout({
            refetchQueries: [{query: auth}]
        });
    }

    renderButtons() {
        const {loading, currentUser} = this.props.auth;

        if (loading) {
            return <div/>;
        }

        if (currentUser) {
            return <li><a onClick={this.onLogoutClick.bind(this)}>Logout</a></li>;
        }

        return (
            <div>
                <li>
                    <Link to="signup">Signup</Link>
                </li>
                <li>
                    <Link to="login">Login</Link>
                </li>
            </div>
        )

    }

    render() {
        return (
            <nav>
                <span>
                    <Link to="/">
                        Home
                    </Link>
                </span>
                <ul>
                    {this.renderButtons()}
                </ul>
            </nav>
        );
    }
}

Header.propTypes = {
    logout: PropTypes.func.isRequired,
    auth: PropTypes.shape({
        loading: PropTypes.bool,
        currentUser: PropTypes.object
    }).isRequired
};

export default compose(
    graphql(logout, {name: 'logout'}),
    graphql(auth, {name: 'auth'})
)(Header)