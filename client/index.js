import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory'
import {ApolloProvider} from 'react-apollo';
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import App from './components/app';

const client = new ApolloClient({
    link: new HttpLink({ credentials: 'same-origin' }),
    cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
    dataIdFromObject: o => o.id
});

const store = createStore(
    reducers,
    compose(
        applyMiddleware(),
        // If you are using the devToolsExtension, you can add it here also
        (typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined') ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f,
    )
);

const Root = () => {
    return (
        <ApolloProvider store={store} client={client}>
            <App />
        </ApolloProvider>
    );
};

ReactDOM.render(<Root/>, document.querySelector('#app'));
