
const initialState = {
  randomSelector:"this is so i can see if it is working"
};

export default function(state = initialState, action) {
    switch (action.type) {
        case 'FETCH_USER':
            return action.payload;
        default:
            return state;
    }
}