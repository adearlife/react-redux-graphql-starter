var path = require('path');

module.exports = {
    entry: {
        index: './client/index.js'
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: 'build.js'
    },
    devServer: {
        historyApiFallback: true,
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.s?css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                use: ['babel-loader' ,'eslint-loader'],
                exclude: /node_modules/
            }
        ]
    }
};